function JSUploader() {
    this.myFyle = {};
    var baseClass = this;

    this.addFiles = function(files) {
        $.each(files, function(i, file) {
            var temp = {file: file, progressTotal: 0, progressDone: 0, element: null, valid: false};

            temp.valid = (file.type == 'text/csv') && file.size / 1024 / 1024 < 2;

            baseClass.myFyle = temp;
        });
    };


    this.uploadFile =  function() {
        var file = baseClass.myFyle;
        

        if(file.valid) {
            var data = new FormData();
            data.append('uploadFile', file.file);

            $.ajax({
                url: '/',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (response) {
                    setTimeout(getResult, 2000);
                }
                ,
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();

                    if (xhr.upload) {
                        console.log('xhr upload');

                        xhr.upload.onprogress = function (e) {
                            $("#mae").html("uploading");
                            $("#rmape").html("uploading");
                        };
                    }

                    return xhr;
                }
            });
        }
    };

    
}

function getResult(){
    $.ajax({
        method: "POST",
        url: "/result"
        //,
        /*beforeSend: function( xhr ) {
            xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
        }*/
    })
    .done(function( data ) {
        $("#mae").html(data.mae);
        $("#rmape").html(data.rmape);
        console.log(data);
    })
    .fail(function() {
        alert( "error" );
    });
}

var uploader = new JSUploader();

$(document).ready(function()
{

    $("#upload").click(function() {
        $("#uploadFiles").change();
        uploader.uploadFile();
    })

    $("#uploadFiles").change(function() {
        var files = this.files;
        uploader.addFiles(files);
        //uploader.uploadFile();
    });

});

