const util = require('util');
const exec = util.promisify(require('child_process').exec);

exports.res = {
  status: "NR",
  mae: undefined,
  rmape: undefined
}

exports.debugls = async function() {
  let command = "ls";
  const { stdout, stderr } = await exec(command);
  console.log('stdout:', stdout);
  console.log('stderr:', stderr);
}

exports.runMAE = async function(filepath) {
  let command = './metrics/mae.out ' + "metrics/answers.csv " + filepath + " metrics/indexes.csv";
  console.log("command: "+command);
  const { stdout, stderr } = await exec(command);
  console.log('stdout:', stdout);
  console.log('stderr:', stderr);
  exports.res.mae = stdout;
}

exports.runRMAPE = async function(filepath) {
  let command = './metrics/rmape.out ' + "metrics/answers.csv " + filepath + " metrics/indexes.csv";
  console.log("command: "+command);
  const { stdout, stderr } = await exec(command);
  console.log('stdout:', stdout);
  console.log('stderr:', stderr);
  exports.res.rmape = stdout;
}

exports.test = async function(filepath) {
  await exports.runMAE(filepath);
  await exports.runRMAPE(filepath);

  return exports.res;
}


module.export = exports; 