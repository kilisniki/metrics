var express = require('express'),
    router = express.Router(),
    fs = require("fs"),
    multiparty = require('multiparty');
    metr = require('../metrics/metr.js');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Metrics' });
});


router.post('/', function(req, res, next) {
    var form = new multiparty.Form();
    var uploadFile = {uploadPath: '', type: '', size: 0};
    var maxSize = 20 * 1024 * 1024; //20MB
    var supportMimeTypes = ['text/csv'];
    var errors = [];

    form.on('error', function(err){
        if(fs.existsSync(uploadFile.path)) {
            fs.unlinkSync(uploadFile.path);
            console.log('error');
        }
    });

    form.on('close', function() {
        if(errors.length == 0) {    		
        	//console.log(uploadFile.path);
     		var rt = metr.test('files/'+uploadFile.name);

     		res.send({status: 'ok', text:'ok 123'})    
     		console.log("complete: "+JSON.stringify(rt));

   		} 
        else {
            if(fs.existsSync(uploadFile.path)) {
                fs.unlinkSync(uploadFile.path);
            }
            res.send({status: 'bad', errors: errors});
        }
    });

    // listen on part event 
    form.on('part', function(part) {
        uploadFile.size = part.byteCount;
        uploadFile.type = part.headers['content-type'];
        uploadFile.name = part.filename;
        uploadFile.path = './files/' + part.filename;

        if(uploadFile.size > maxSize) {
            errors.push('File size is ' + uploadFile.size / 1024 / 1024 + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
        }

        if(supportMimeTypes.indexOf(uploadFile.type) == -1) {
            errors.push('Unsupported mimetype ' + uploadFile.type);
        }

        if(errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path);
            part.pipe(out);
        }
        else {
            part.resume();
        }
    });

    // parse the form
    form.parse(req);
});

router.get('/result', function(req, res) {
	res.json(metr.res);
});

router.post('/result', function(req, res) {
	res.json(metr.res);
});

module.exports = router;